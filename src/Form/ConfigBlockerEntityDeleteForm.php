<?php

namespace Drupal\client_config_care\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Config blocker entity entities.
 *
 * @ingroup client_config_care
 */
class ConfigBlockerEntityDeleteForm extends ContentEntityDeleteForm {


}
